package org.lq.springbootmongodb.repository;

import org.lq.springbootmongodb.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User,String> {
}
