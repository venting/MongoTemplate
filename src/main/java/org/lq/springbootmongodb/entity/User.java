package org.lq.springbootmongodb.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document("User")
@Accessors(chain = true)
public class User {
    @Id
    private String id;
    private String name;
    private Integer age;
    private String email;
    private Date createDate;
}
