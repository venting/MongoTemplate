package org.lq.springbootmongodb;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.junit.jupiter.api.Test;
import org.lq.springbootmongodb.entity.User;
import org.springframework.beans.factory.NamedBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.Date;
import java.util.List;
import java.util.Queue;
import java.util.regex.Pattern;

@SpringBootTest
class SpringbootMongodbApplicationTests {
    @Autowired
    private MongoTemplate mongoTemplate;
    //添加操作
    @Test
    public void create(){
        User user1 = mongoTemplate.insert(new User().setAge(20).setName("ming").setEmail("123.@qq.com").setCreateDate(new Date()));
        System.out.println(user1);
    }
    //查询表中所有数据
    @Test
    public void findAll(){
        List<User> userList = mongoTemplate.findAll(User.class);
        userList.forEach(System.out::println);
    }
    //通过id查询
    @Test
    public void findById(){
        User user = mongoTemplate.findById("612f1bae0e0b6331080c9c9a", User.class);
        System.out.println("--------------"+user);
    }
    //条件查询
    @Test
    public void findUserList(){
        //name = ming   and age  =20
        Query query = new Query(Criteria.where("name").is("ming").and("age").is(20));
        List<User> users = mongoTemplate.find(query, User.class);
        System.out.println(users);
    }
    //模糊查询

    @Test
    public void findUserLikeList(){
        String name = "ing";
        String regex = String.format("%s%s%s",".*",name,".*$");
        Pattern pattern = Pattern.compile(regex,Pattern.CASE_INSENSITIVE);
        Query queue = new Query(Criteria.where("name").regex(pattern));
        List<User> users = mongoTemplate.find(queue, User.class);
        users.forEach(System.out::println);

    }
    //分页查询
    @Test
    public void findByPages(){
        //条件
        String name = "ing";
        String regex = String.format("%s%s%s",".*",name,".*$");
        Pattern pattern = Pattern.compile(regex,Pattern.CASE_INSENSITIVE);
        Query queue = new Query(Criteria.where("name").regex(pattern));
        //分页
        int pageNo = 1;
        int  pageSize = 3;
        //查询总行数
        long count = mongoTemplate.count(queue, User.class);
        List<User> users = mongoTemplate.find(queue.skip((pageNo - 1) * pageSize).limit(pageSize), User.class);
        System.out.println("count------------->"+count);
        System.out.println("users-------------->"+users);

    }
    //修改数据
    @Test
    public void updateUser(){
        //根据id查询
        User user = mongoTemplate.findById("612f1bae0e0b6331080c9c9a", User.class);
        //设置修改值
        user.setName("ming_1").setAge(50).setEmail("abc@qq.com");
        Query query = new Query(Criteria.where("_id").is(user.getId()));
        Update update = new Update();
        update.set("name", user.getName()).set("age", user.getAge()).set("email", user.getEmail());
        UpdateResult upsert = mongoTemplate.upsert(query, update, User.class);
        long modifiedCount = upsert.getModifiedCount();
        System.out.println("------------"+modifiedCount);
    }
    //删除操作
    @Test
    public void delteUser(){
        Query query = new Query(Criteria.where("_id").is("612f1bae0e0b6331080c9c9a"));

        DeleteResult remove = mongoTemplate.remove(query, User.class);
        long deletedCount = remove.getDeletedCount();
        System.out.println(deletedCount);
    }
}
