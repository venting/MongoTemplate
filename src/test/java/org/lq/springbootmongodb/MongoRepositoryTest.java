package org.lq.springbootmongodb;
import org.junit.jupiter.api.Test;
import org.lq.springbootmongodb.entity.User;
import org.lq.springbootmongodb.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.*;

import javax.xml.transform.Source;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class MongoRepositoryTest {
    @Autowired
    private UserRepository userRepository;
    //添加操作
    @Test
    public void create(){
        User u = new User().setAge(21).setName("lucy").setEmail("123@qq.com").setCreateDate(new Date());
        User user = userRepository.save(u);
        System.out.println("----------------"+user);
    }
    //查询表中所有数据
    @Test
    public void findAll(){
        List<User> all = userRepository.findAll();
        all.forEach(System.out::println);
    }
    //通过id查询
    @Test
    public void findById(){
        User user = userRepository.findById("612f472a21cf250fb2054c6b").get();
        System.out.println(user);
    }
    //条件查询
    @Test
    public void findUserList(){
        //名称是lucy 年龄是 21
        User user = new User().setAge(21).setName("lucy");
        Example<User> userExamle = Example.of(user);
        List<User> all = userRepository.findAll(userExamle);
        System.out.println(all);
    }
    //模糊查询
    @Test
    public void findUserLikeList(){
        //设置模糊查询的匹配规则
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING)
                .withIgnoreCase(true);
        User user = new User().setAge(21).setName("l");
        Example<User> userExamle = Example.of(user,matcher);
        List<User> all = userRepository.findAll(userExamle);
        System.out.println(all);
    }
    //分页查询
    @Test
    public void findByPages(){
        //设置分页参数
        //0代表   第一页
        Pageable pageable = PageRequest.of(0, 3);
        User u = new User();
        u.setName("mary");
        Example<User> userExample = Example.of(u);
        Page<User> page = userRepository.findAll(userExample,pageable);
        System.out.println(page);
    }
    //修改数据
    @Test
    public void updateUser(){
        User user = userRepository.findById("612f472a21cf250fb2054c6b").get();
        user.setAge(100);
        user.setName("修改的名字");
        user.setEmail("菜单邮箱@qq.com");
        User u = userRepository.save(user);
        System.out.println(u);

    }
    //删除操作
    @Test
    public void delteUser(){
        userRepository.deleteById("612f472a21cf250fb2054c6b");
    }
}
